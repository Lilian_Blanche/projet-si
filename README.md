# Projet-SI

## services
***

pour lancer le service :
1. vérifier qu'il y a le fichier "webservice.jar" dans le dossier "librable"
2. si non faire une package du projet et copier/coller le fichier "webservice.jar" du dossier "target"
3. exécuter la commande suivante :

```
docker-compose up -d
```

4. si vous avez besoin de refaire un nouveau package utiliser la commande suivante avant de up les services :

```
docker-compose build
```

## base mongo
***

On alimente la bd mongo grâce au script "init.js".

executer les commandes suivantes pour tester :
```
docker-compose down
docker-compose up -d
docker exec -it projet-si-mongodb bash
mongo
show dbs;
use risques;
show collections;
```
